import torch
import torch.nn as nn
from torchvision import datasets, transforms, models
import os
import numpy as np
import torch.optim as optim
import time
import Model

# Paths to the training and validation data directories
train_path = os.path.join('gendataset' + '/Train')
val_path = os.path.join('gendataset' + '/Valid')

# Define transformations for data augmentation and normalization
train_transform = transforms.Compose([
    transforms.RandomResizedCrop(224, scale=(0.8, 1.0)),
    transforms.RandomHorizontalFlip(),
    transforms.RandomVerticalFlip(),
    transforms.RandomRotation(degrees=30),  # Increased rotation
    transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.2),
    transforms.RandomPerspective(distortion_scale=0.5),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

valid_transform = transforms.Compose([
    transforms.Resize(224),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

# Load datasets using ImageFolder and apply transformations
train_data = datasets.ImageFolder(train_path, transform=train_transform)
val_data = datasets.ImageFolder(val_path, transform=valid_transform)

# Create data loaders for training and validation sets
train_loader = torch.utils.data.DataLoader(train_data, batch_size=271, shuffle=True)
val_loader = torch.utils.data.DataLoader(val_data, batch_size=271, shuffle=True)

# Use GPU if available, otherwise use CPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Re-create the model from the saved checkpoint
net = Model.Net().to(device)
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters(), lr=0.001)

validation_loss = []  # List to store validation losses during training

start_time = time.time()
best_loss = float('inf')  # Initialize best_loss with infinity
for epoch in range(60):  # Loop over the dataset multiple times
    running_loss = 0.0
    for i, data in enumerate(train_loader, 0):
        inputs, labels = data[0].to(device), data[1].to(device)

        optimizer.zero_grad()  # Zero the parameter gradients

        outputs = net(inputs)  # Forward pass
        loss = criterion(outputs, labels)  # Calculate the loss
        loss.backward()  # Backward pass
        optimizer.step()  # Update weights

        running_loss += loss.item()  # Accumulate loss for each batch
    epoch_loss = running_loss / (i + 1)  # Calculate average loss per epoch
    print("Epoch: ", epoch, " train loss: ", '%.3f' % epoch_loss)

    # Validation phase
    net.eval()  # Set model to evaluation mode
    with torch.no_grad():
        running_loss = 0.0
        for i, data in enumerate(val_loader, 0):
            inputs, labels = data[0].to(device), data[1].to(device)

            outputs = net(inputs)
            loss = criterion(outputs, labels)

            running_loss += loss.item()  # Accumulate loss for validation set
        epoch_loss = running_loss / (i + 1)

        validation_loss.append(epoch_loss)  # Store validation loss

        print("Epoch: ", epoch, " validation loss: ", '%.3f' % epoch_loss)

        if epoch_loss < best_loss:
            best_loss = epoch_loss  # Update best loss if current epoch's loss is better

        running_avg_loss = np.mean(validation_loss[-10:])  # Calculate running average loss
        if running_avg_loss <= epoch_loss and epoch > 50:  # Implement early stopping
            print("Stopping Early")
            break

    net.train()  # Set model back to training mode after validation phase

time_elapsed = (time.time() - start_time) // 60  # Calculate training time in minutes
print('Finished Training in %d mins' % time_elapsed)

# Save model checkpoint with relevant information
checkpoint = {
    'input_size': 224,
    'output_size': 2,
    'state_dict': net.state_dict(),
    'optimizer_state_dict': optimizer.state_dict(),
    'criterion': criterion,
    'epochs': 30,
    'class_to_idx': train_data.class_to_idx
}

torch.save(checkpoint, 'checkpoint.pth')  # Save the checkpoint