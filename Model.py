import torch.nn as nn




# Define the neural network architecture
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        # Convolutional layers
        self.conv1 = nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1)  # Output size: 16x224x224
        self.relu = nn.ReLU()
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0)  # Output size: 16x112x112
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)  # Output size: 32x112x112
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0)  # Output size: 32x56x56
        self.conv3 = nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=0)  # Output size: 32x54x54
        self.conv4 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)  # Output size: 64x54x54
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0)  # Output size: 64x27x27
        self.conv5 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)  # Output size: 64x27x27
        self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0)  # Output size: 64x13x13
        self.bn1 = nn.BatchNorm2d(16)
        self.bn2 = nn.BatchNorm2d(32)
        self.bn3 = nn.BatchNorm2d(32)
        self.bn4 = nn.BatchNorm2d(64)
        self.bn5 = nn.BatchNorm2d(64)

        # Fully connected layers
        self.fc1_input_size = 32 * 54 * 54  # Calculate input size for linear1
        self.fc1 = nn.Linear(self.fc1_input_size, 512)
        self.fc2 = nn.Linear(512, 128)
        self.fc3 = nn.Linear(128, 2)

    def forward(self, x):
        # Forward pass through the network
        x = self.conv1(x)
        x = self.relu(x)
        x = self.pool1(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.pool2(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = x.view(-1, self.fc1_input_size)  # Reshape before passing to fully connected layers
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        x = self.relu(x)
        x = self.fc3(x)
        return x
