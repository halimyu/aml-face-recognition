import torch
import torchvision
from matplotlib import pyplot as plt
from torchvision import datasets, transforms, models
import os
import numpy as np
import torch.nn as nn
import torch.optim as optim
import Model

# Paths to the test data directory
test_path = os.path.join('gendataset' + '/Test')

# Define transformations for test data
test_transform = transforms.Compose([
    transforms.Resize(224),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

# Load test dataset using ImageFolder and apply transformations
test_data = datasets.ImageFolder(test_path, transform=test_transform)
test_loader = torch.utils.data.DataLoader(test_data, batch_size=8, shuffle=True)

# Class names for the dataset
classes = ('Male', 'Female')

# Check if GPU is available, otherwise use CPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Load the pre-trained model checkpoint
checkpoint = torch.load('checkpoint.pth')

# Re-create the model from the saved checkpoint
model = Model.Net()
model.load_state_dict(checkpoint['state_dict'])
model.class_to_idx = checkpoint['class_to_idx']
model.to(device)

# Function to display images
def imshow(img):
    img = img / 2 + 0.5  # Unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

# Get a batch of test images
dataiter = iter(test_loader)
images, labels = next(dataiter)

# Display the test images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(8)))

# Test the model on the test dataset
with torch.no_grad():
    outputs = model(images.to(device))
    _, predicted = torch.max(outputs, 1)

# Print the predicted labels
print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(8)))

# Calculate the accuracy of the model on the test dataset
test_loader = torch.utils.data.DataLoader(test_data, batch_size=100, shuffle=False)
correct = 0
total = 0
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        outputs = model(images.to(device))
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels.to(device)).sum().item()

# Print the accuracy
print('Accuracy of the network on the 542 test images: %.3F %%' % (100 * correct / total))
